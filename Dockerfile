FROM jrottenberg/ffmpeg:4.0-ubuntu

RUN apt-get update && \
    apt-get install nodejs npm -y

RUN ffmpeg -version

WORKDIR /app/

COPY ./src/* ./

RUN npm install

ENTRYPOINT nodejs sample.js
